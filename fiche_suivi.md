# Tracking Sheet 
### Alexandra CHATON 
- login : chatona
- mail : alexandra.chaton@etu.univ-grenoble-alpes.fr

### Thomas FRION
- login : friont
- mail : thomas.frion@etu.univ-grenoble-alpes.fr

## Week 1 (20/01 - 26/01)
Discovery of the differents projects

Our project : PGHM rescue application

In co-operation with Olivier Favre.

## Week 2 (27/01 - 02/02)
We attend the course about the LoRaWAN technology.
We were waiting to have some news about the project from Olivier Favre.

## Week 3 (03/02 - 09/02)
Meeting with Olivier Favre.

Discussion about the objectives of the project and the expectations of the clients.

The technologies we will used are still are not entirely specified.

### TODO :
- Take a look at Leaflet, Django, Sphinx, Angular, TypeScript
- Read the documents about [CHOUCAS](http://choucas.ign.fr)

## Week 4 (10/02 - 16/02 )
Waiting to know about the technologies we will use.

Reading about Django and Python.

## Week 5 (17/02 - 23/02)
First reading of the source code from the already existing application.

### TODO :
- ~~write the SRS~~
- ~~start writing the README and CHANGELOG~~
- ~~write the wiki of our project~~

## Week 6 (02/03 - 08/03)
Installation of pre-existing tools DONE.

Redaction of the SRS IN PROGRESS.

Consideration of the structure of the API.

### TODO :
- ~~redaction of the README~~
- ~~finish the redaction of the SRS~~
- ~~start designing the API~~
- ~~decide the architecture of our application~~

## Week 7 (09/03 - 15/03)

* Mid-term presentation of the project 
* (2020-03-12) Appointment with Olivier FAVRE to discuss some points of the project. The notes are available [here](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/8/docs/-/blob/master/Other-documents/spec.txt) in French

## Week 8 (16/03 - 22/03)

* Writing a snippet to explain how to fix the version error that came up during the installation of the Django project 
* Initialization of the **new** Rest API project with Django (back-end)
* Initiation of the Angular project (front-end)

 
Alexandra had to restart all the installations because of computer problems. She couldn't work on the project the last two weeks.

## Week 9 (23/03 - 29/03)
Front-end :
* Initialisation of components in 4 zones:
    * sidebar-left
    * sidebar-right
    * coord-map
    * map


Back-end :
* Fix the problem connected to an update of Django
    * After the definition of the array MIDDLEWARE_CLASSES add MIDDLEWARE = MIDDLEWARE_CLASSES
    * Problem with the loading of the module choucas.wsgi.application => Either comment out or delete the line django.contrib.auth.middleware.SessionAuthenticationMiddleware
* Installation (to test) of the module : django_rest_swagger which can document automatically the API
* Test to understand how the django_rest_api framework works with the creation of the feature intended for recovering all the users (GET http://127.0.0.1/users/)

### TODO :
* Front-end :
    * Finish the writing of the components
    * Finish to display the map

* Back-end :
    * ~~Finish the conception of the model (simple version for now)~~
    * ~~Modify the feature previously described to integrate the new model~~
    * ~~Add routes for the API~~
    * ~~Write the documentation~~

## Week 10 (30/03 - 05/04)
* Front-end : big problems with internet

* Back-end :
    * Creation of the 'link' between a user (gendarme) and its unit.
    * Attempt to do something a little bit cleaner ==> 
	    * Create a PghmUser class that inherits of User (default class). 
	    * Then add a foreign key field that points to the unit.
	    * Status: new class and field created, attempt to change the default column of foreign key but it remains to modify the forms of the admin part to take into account the change.

### TODO: 
* ~~Create Read Update Delete (CRUD) on units and users (API Part)~~ 
* ~~doc generation of what will have been done since then.~~

## Week 11 (06/04 - 12/04)
* Front-end :
    * zone 1: done (part with js to work with Thomas)
    * zone 2: working on it (part with js to work with Thomas)
    * zone 3 & 4: to do
* Back-end :
    * API: CRUD for users     
    * Adding the unit field in user administration forms
    * Creation of unit administration interfaces
    * Details about creation of a user via the administration interface: you have to enter the username, password, password confirmation (this is all you had before)  + you can enter the first name, last name and email of the new user and you have to select his PGHM unit.

### 2020-04-10
* Back-end:
    * API For units:
        * GET | POST | UPDATE | DELETE
        * A unit can be selected by its code
    * Refactoring of views' code
    * Implementation of automatic document generation

## Week 12 (13/04 - 19/04)

Unfortunately, we were not able to work on the project because we were quite busy with the multiple project reports that were due for this week as part of our ongoing assessment.


## Week 13 (20/04 - 26/04)
Alexandra:
    Call with Olivier Favre about the code of the Front-end and the expectations about it.

### 2020-04-21

* Writing of the report

## Week 14 (27/04 - 30/04)

### 2020-04-29
* Front end :
    * Merge of different part
    * Working on managing the aspect of Single Page Application with an authentification part

* Backend:
    * Add unit's settigns system 

### 2020-04-30

* Backend:
    * Add security part (with JWT)
    * Add more details/information in API documentation


## IN THE FUTURE- TODO
* Front-end:
    * Finish the authentification process
    * Understanding the code in JavaScript of the prototype version to finish components
    * Optimization of the code
* Back-end:
    * Add more requested features (with models or not)
    * Improve serializer validator for adding users and units:
        * To add or update a user (via tha API), we had to disable the validators
    * Add a management of the different extern resources (extern API such as IGN)
    * Add a management of spacial and/or geographical components
* Connexion between back-end et front-end




