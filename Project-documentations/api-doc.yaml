swagger: '2.0'
info:
  title: PGHM API Documentation
  description: Test description
  termsOfService: https://www.google.com/policies/terms/
  contact:
    email: thomas.frion@protonmail.com
  license:
    name: BSD License
  version: v1
host: localhost:8000
schemes:
  - http
basePath: /
consumes:
  - application/json
produces:
  - application/json
securityDefinitions:
  Bearer:
    type: apiKey
    name: Authorization
    in: header
    description: Authentication uses a JSON Web Token (JWT). To get your JWT you must
      use the /token resource.
security:
  - Bearer: []
paths:
  /token/:
    post:
      operationId: token_create
      description: |-
        Takes a set of user credentials and returns an access and refresh JSON web
        token pair to prove the authentication of those credentials.
      parameters:
        - name: data
          in: body
          required: true
          schema:
            $ref: '#/definitions/TokenObtainPair'
      responses:
        '201':
          description: ''
          schema:
            $ref: '#/definitions/TokenObtainPair'
      tags:
        - token
    parameters: []
  /token/refresh:
    post:
      operationId: token_refresh_create
      description: |-
        Takes a refresh type JSON web token and returns an access type JSON web
        token if the refresh token is valid.
      parameters:
        - name: data
          in: body
          required: true
          schema:
            $ref: '#/definitions/TokenRefresh'
      responses:
        '201':
          description: ''
          schema:
            $ref: '#/definitions/TokenRefresh'
      tags:
        - token
    parameters: []
  /units/:
    get:
      operationId: units_list
      description: ''
      parameters: []
      responses:
        '200':
          description: ''
          schema:
            type: array
            items:
              $ref: '#/definitions/Unit'
      tags:
        - units
    post:
      operationId: units_create
      description: ''
      parameters:
        - name: data
          in: body
          required: true
          schema:
            $ref: '#/definitions/Unit'
      responses:
        '201':
          description: ''
          schema:
            $ref: '#/definitions/Unit'
      tags:
        - units
    parameters: []
  /units/settings/{code}:
    get:
      operationId: units_settings_read
      description: ''
      parameters: []
      responses:
        '200':
          description: ''
          schema:
            type: array
            items:
              $ref: '#/definitions/SettingUnit'
      tags:
        - units
    parameters:
      - name: code
        in: path
        required: true
        type: string
  /units/{code}:
    get:
      operationId: units_read
      description: ''
      parameters: []
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PghmUser'
      tags:
        - units
    put:
      operationId: units_update
      description: ''
      parameters:
        - name: data
          in: body
          required: true
          schema:
            $ref: '#/definitions/Unit'
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/Unit'
      tags:
        - units
    delete:
      operationId: units_delete
      description: ''
      parameters: []
      responses:
        '204':
          description: This should not crash (response object with no schema)
      tags:
        - units
    parameters:
      - name: code
        in: path
        required: true
        type: string
  /units/{id}:
    get:
      operationId: units_read
      description: ''
      parameters: []
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PghmUser'
      tags:
        - units
    put:
      operationId: units_update
      description: ''
      parameters:
        - name: data
          in: body
          required: true
          schema:
            $ref: '#/definitions/Unit'
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/Unit'
      tags:
        - units
    delete:
      operationId: units_delete
      description: ''
      parameters: []
      responses:
        '204':
          description: This should not crash (response object with no schema)
      tags:
        - units
    parameters:
      - name: id
        in: path
        required: true
        type: string
  /users/:
    get:
      operationId: users_list
      description: ''
      parameters: []
      responses:
        '200':
          description: ''
          schema:
            type: array
            items:
              $ref: '#/definitions/PghmUser'
      tags:
        - users
    post:
      operationId: users_create
      description: ''
      parameters:
        - name: data
          in: body
          required: true
          schema:
            $ref: '#/definitions/PghmUser'
      responses:
        '201':
          description: ''
          schema:
            $ref: '#/definitions/PghmUser'
      tags:
        - users
    parameters: []
  /users/{id}:
    get:
      operationId: users_read
      description: ''
      parameters: []
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PghmUser'
      tags:
        - users
    put:
      operationId: users_update
      description: ''
      parameters:
        - name: data
          in: body
          required: true
          schema:
            $ref: '#/definitions/PghmUser'
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PghmUser'
      tags:
        - users
    delete:
      operationId: users_delete
      description: ''
      parameters: []
      responses:
        '204':
          description: This should not crash (response object with no schema)
      tags:
        - users
    parameters:
      - name: id
        in: path
        required: true
        type: string
definitions:
  TokenObtainPair:
    required:
      - username
      - password
    type: object
    properties:
      username:
        title: Username
        type: string
        minLength: 1
      password:
        title: Password
        type: string
        minLength: 1
  TokenRefresh:
    required:
      - refresh
    type: object
    properties:
      refresh:
        title: Refresh
        type: string
        minLength: 1
  SettingUnit:
    required:
      - name
      - value
      - unit
    type: object
    properties:
      id:
        title: ID
        type: integer
        readOnly: true
      name:
        title: Name
        type: string
        enum:
          - Zoom
          - Test
      value:
        title: Value
        type: string
        maxLength: 1000
        minLength: 1
      unit:
        title: Unit
        type: string
  Unit:
    required:
      - code
      - nom
      - email
    type: object
    properties:
      code:
        title: Code
        type: string
        maxLength: 16
        minLength: 1
      nom:
        title: Nom
        type: string
        maxLength: 64
        minLength: 1
      email:
        title: Email
        type: string
        format: email
        maxLength: 64
        minLength: 1
      geom:
        title: Geom
        type: string
        x-nullable: true
      dept:
        title: Dept
        type: string
        maxLength: 3
        x-nullable: true
      settings:
        type: array
        items:
          $ref: '#/definitions/SettingUnit'
        readOnly: true
  User:
    title: User
    required:
      - password
      - username
    type: object
    properties:
      id:
        title: ID
        type: integer
        readOnly: true
      password:
        title: Password
        type: string
        maxLength: 128
        minLength: 1
      last_login:
        title: Last login
        type: string
        format: date-time
        x-nullable: true
      is_superuser:
        title: Superuser status
        description: Designates that this user has all permissions without explicitly
          assigning them.
        type: boolean
      username:
        title: Username
        description: Required. 150 characters or fewer. Letters, digits and @/./+/-/_
          only.
        type: string
        maxLength: 150
        minLength: 1
      first_name:
        title: First name
        type: string
        maxLength: 30
      last_name:
        title: Last name
        type: string
        maxLength: 150
      email:
        title: Email address
        type: string
        format: email
        maxLength: 254
      is_staff:
        title: Staff status
        description: Designates whether the user can log into this admin site.
        type: boolean
      is_active:
        title: Active
        description: Designates whether this user should be treated as active. Unselect
          this instead of deleting accounts.
        type: boolean
      date_joined:
        title: Date joined
        type: string
        format: date-time
      groups:
        description: The groups this user belongs to. A user will get all permissions
          granted to each of their groups.
        type: array
        items:
          description: The groups this user belongs to. A user will get all permissions
            granted to each of their groups.
          type: integer
        uniqueItems: true
      user_permissions:
        description: Specific permissions for this user.
        type: array
        items:
          description: Specific permissions for this user.
          type: integer
        uniqueItems: true
  PghmUser:
    required:
      - unit
      - user
    type: object
    properties:
      id:
        title: ID
        type: integer
        readOnly: true
      unit:
        $ref: '#/definitions/Unit'
      user:
        $ref: '#/definitions/User'
