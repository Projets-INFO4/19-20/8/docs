# CHOUCAS Project

Over 15000 mountain rescue request by year are recorded in France. The Pelotons de Gendarmerie de Haute Montagne (PGHM) are the main mountain rescue teams.
In this context, CHOUCAS project aims to provide methods and tools to make and improve geographic data from different sources, and thinking models to help the decision process of locating victims in mountainous environment. The goal is to improve the response alert capability.

Following principles of CHOUCAS project, the application will be rewritten.

The application aims to connect:
* The applicant: person who launch the alert, who could be the victim or a third party.
*  The rescue team

The objective of this relation is to get the victim' localization from a manual research of geographic hints from different data sources as well as on the knowledge of the rescue team. This hint research is part of a process of reasoning, which consists of formulating hypotheses based on the initial information provided by the applicant, refining them gradually through dialogue and data analysis, and then deducing the most probable location of the victim.

The rescue application will therefore also have the task of assisting in locating the victim by providing the tools to manage the data necessary for the rescuers' decision-making: multi-source data (institutional or community data) and multi-format data (maps, topo-guides for hikes in paper format, vector data). 

This application, although the initiative of the Grenoble PGHM, should have a national scope so that all PGHMs in France can use it.


>Our tracking sheet is available [here](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/8/docs/-/blob/master/fiche_suivi.md) (French version only)
