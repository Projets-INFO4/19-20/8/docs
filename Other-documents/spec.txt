Cahier des charges - projet

Back:
* Authentification
	* Utilisateur (nom, prénom, email, permissions sur les objets éditables, préférences IHM)
	* permissions
	* JWT
* Paramètres de l'utilisateur
* Unité (du PGHM)
	* Va définir les cartes à utiliser
* Administration
	* Unité (code, nom, geom_unite, geom_competence, tel, email)
	* Objets en fonction des droits de l'utilisateur (CRUD)

Front:
* Init des cartes + outils (boutons & search bar)
* Overlay:
	* Couche Unité
	* Couche tracking
	* Webcam
	* DZ (Drop Zone)
	* Saisie de coordonnées
	* Quand double clique => Affichage Altitude et Gendarmerie compétente

Si on a le temps:
Gendloc:
	* Envoie SMS => Récupération coordonnées GPS pour affichage
